# Minesweeper

Minesweeper but on console.  
Use:  
- **open x y** to open the cell with coordinates (x,y)  
- **flag x y** to mark/unmark the cell with coordinates (x,y)  

![screenshot](https://gitlab.com/Patsomir/MineSweaper/-/raw/master/screenshots/minesweeper.jpg)
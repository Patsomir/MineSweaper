package gamecomponents;

public class MineBox {
    private boolean mine;
    private boolean flag;
    private boolean open;
    private int dangerLevel;

    MineBox() {
        mine = false;
        flag = false;
        open = false;
        dangerLevel = 0;
    }

    void increaseDangerLevel() {
        dangerLevel++;
    }

    boolean placeMine() {
        if (!mine) {
            mine = true;
            return true;
        }
        return false;
    }

    boolean toggleFlag() {
        if (!open) {
            flag = !flag;
            return true;
        }
        return false;
    }

    boolean openBox() {
        if (!open && !flag) {
            open = true;
            return true;
        }
        return false;
    }

    boolean hasMine() {
        return mine;
    }

    boolean hasFlag() {
        return flag;
    }

    boolean isOpen() {
        return open;
    }

    int getDangerLevel() {
        return dangerLevel;
    }

    void print() {
        if (flag) {
            System.out.print('X');
        } else if (!open) {
            System.out.print('\u25A0');
        } else if (!mine) {
            if (dangerLevel == 0) {
                System.out.print('\u25A1');
            } else {
                System.out.print(dangerLevel);
            }
        } else {
            System.out.print('\u2666');
        }
    }
}

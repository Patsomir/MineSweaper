package gamecomponents;

public class MineField {
    private int width;
    private int heigth;
    private MineBox[][] box;
    private int mineCount;
    private int openedBoxesCount;
    private boolean explosion;

    public MineField(int width, int heigth) {
        this.heigth = heigth;
        this.width = width;
        box = new MineBox[heigth][width];
        for (int i = 0; i < heigth; i++) {
            for (int j = 0; j < width; j++) {
                box[i][j] = new MineBox();
            }
        }
        mineCount = 0;
        openedBoxesCount = 0;
        explosion = false;
    }

    public boolean placeMine(int x, int y) {
        if (box[y][x].placeMine()) {
            mineCount++;
            for (int i = y - 1; i <= y + 1; i++) {
                for (int j = x - 1; j <= x + 1; j++) {
                    if (i >= 0 && i <= heigth - 1 && j >= 0 && j <= width - 1) {
                        box[i][j].increaseDangerLevel();
                    }
                }
            }
            return true;
        }
        return false;
    }

    public boolean toggleFlag(int x, int y) {
        if (box[y][x].toggleFlag()) {
            return true;
        }
        return false;
    }

    public boolean openBox(int x, int y) {
        if (box[y][x].openBox()) {
            openedBoxesCount++;
            if (box[y][x].hasMine()) {
                detonateMines();

            } else {
                openSafeBoxes(x, y);
            }
            return true;
        }
        return false;
    }

    private void openSafeBoxes(int x, int y) {
        if (box[y][x].openBox()) {
            openedBoxesCount++;
        }

        if (box[y][x].getDangerLevel() != 0) {
            return;
        } else {
            for (int i = y - 1; i <= y + 1; i++) {
                for (int j = x - 1; j <= x + 1; j++) {
                    if (i >= 0 && i <= heigth - 1 && j >= 0 && j <= width - 1) {
                        if (i != y || j != x) {
                            if (!box[i][j].isOpen() && !box[i][j].hasFlag()) {
                                openSafeBoxes(j, i);
                            }
                        }
                    }
                }
            }
        }
    }

    private void detonateMines() {
        for (int i = 0; i < heigth; i++) {
            for (int j = 0; j < width; j++) {
                if (box[i][j].hasMine()) {
                    box[i][j].openBox();
                }
            }
        }
        explosion = true;
    }

    public boolean hasExploded() {
        return explosion;
    }

    int getMineCount() {
        return mineCount;
    }

    public int getOpenedBoxesCount() {
        return openedBoxesCount;
    }

    int getHeight() {
        return heigth;
    }

    int getWidth() {
        return width;
    }
    
    public void print() {
        System.out.print("  ");
        for(int i = 0; i < width; i++) {
            if(i/10 != 0) {
                System.out.print(i/10);
            } else {
                System.out.print(' ');
            }
            
            System.out.print(' ');
        }
        System.out.println();
        
        System.out.print("  ");
        for(int i = 0; i < width; i++) {
            System.out.print(i%10);
            System.out.print(' ');
        }
        System.out.println();
        System.out.print("\u250c ");
        for(int i = 0; i < width; i++) {
            System.out.print('─');
            System.out.print(' ');
        }
        System.out.println('\u2510');
        
        for(int i = 0; i < heigth; i++) {
            System.out.print("| ");
            for(int j = 0; j < width; j++) {
                box[i][j].print();
                System.out.print(' ');
            }
            System.out.print("| " + i);
            System.out.println();
        }
        
        System.out.print("\u2514 ");
        for(int i = 0; i < width; i++) {
            System.out.print('─');
            System.out.print(' ');
        }
        System.out.println('\u2518');
    }
}

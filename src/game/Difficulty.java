package game;

public enum Difficulty {
    BEGINNER(10, 9, 9), INTERMEDIATE(40, 16, 16), ADVANCED(99, 30, 16);

    private int mineCount;
    private int width;
    private int heigth;

    Difficulty(int mineCount, int width, int heigth) {
        this.mineCount = mineCount;
        this.width = width;
        this.heigth = heigth;
    }
    
    int getMineCount() {
        return mineCount;
    }
    
    int getWidth() {
        return width;
    }
    
    int getHeigth() {
        return heigth;
    }
}

package game;

import gamecomponents.MineField;
import input.Command;
import input.Player;

public class MineSweaper {
    private MineField gameBoard;
    private int startingMines;
    private int heigth;
    private int width;
    private Player player;
    private int winCondition;

    MineSweaper() {
        setDifficulty(Difficulty.INTERMEDIATE);
        gameBoard = new MineField(width, heigth);
        player = new Player();
        winCondition = heigth * width - startingMines;
    }

    MineSweaper(Difficulty difficulty) {
        setDifficulty(difficulty);
        gameBoard = new MineField(width, heigth);
        player = new Player();
        winCondition = heigth * width - startingMines;
    }

    private void setDifficulty(Difficulty setting) {
        startingMines = setting.getMineCount();
        heigth = setting.getHeigth();
        width = setting.getWidth();
    }

    private void plantMines(int initialX, int initialY) {
        int mines = startingMines;
        int minedX;
        int minedY;
        while (mines > 0) {
            minedX = (int) (Math.random() * width);
            minedY = (int) (Math.random() * heigth);
            if (minedX != initialX || minedY != initialY) {
                if (gameBoard.placeMine(minedX, minedY)) {
                    mines--;
                }
            }
        }
    }

    private boolean executeCommand(Command command) {
        if ("Invalid Command".equals(command.getAction()) || command.getxCoordinate() >= width
                || command.getyCoordinate() >= heigth) {
            System.out.println("Invalid Command");
            return false;
        } else {
            if ("flag".equals(command.getAction())) {
                if (!gameBoard.toggleFlag(command.getxCoordinate(), command.getyCoordinate())) {
                    System.out.println("Can't toggle flag");
                    return false;
                }
            }
            if ("open".equals(command.getAction())) {
                if (gameBoard.getOpenedBoxesCount() == 0) {
                    plantMines(command.getxCoordinate(), command.getyCoordinate());
                }
                if (!gameBoard.openBox(command.getxCoordinate(), command.getyCoordinate())) {
                    System.out.println("Can't open box");
                    return false;
                }
            }
        }
        return true;
    }

    public void play() {
        System.out.println("Mine Sweaper");
        System.out.println("------------");
        System.out.println();
        while (true) {
            System.out.println("Mine Count: " + startingMines + "  Opened Boxes: " + gameBoard.getOpenedBoxesCount());
            System.out.println();

            gameBoard.print();

            if (gameBoard.hasExploded()) {
                System.out.println("You lose!");
                break;
            }
            if (gameBoard.getOpenedBoxesCount() == winCondition) {
                System.out.println("You win!");
                break;
            }

            Command command;
            do {
                System.out.print('>');
                command = player.parseInput();
            } while (!executeCommand(command));
        }
        System.out.println("Game Over!");
    }

    public static void main(String[] args) {
        MineSweaper hard = new MineSweaper(Difficulty.ADVANCED);
        hard.play();
    }

}

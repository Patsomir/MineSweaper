package input;

import java.util.Scanner;

public class Player {
    private Scanner userInput;

    public Player() {
        userInput = new Scanner(System.in);
    }

    public Command parseInput() {
        String action;
        int x;
        int y;
        String validater;

        String line = userInput.nextLine();
        Scanner parser = new Scanner(line);

        if (parser.hasNext()) {
            action = parser.next();;

            if (parser.hasNext()) {
                validater = parser.next();
                if (isInteger(validater)) {
                    x = Integer.parseInt(validater);
                    
                    if (parser.hasNext()) {
                        validater = parser.next();
                        if (isInteger(validater)) {
                            y = Integer.parseInt(validater);
                            
                            if (!parser.hasNext()) {
                                parser.close();
                                return new Command(action, x, y);
                            }
                        }
                    }
                }
            }
        }

        parser.close();
        return new Command("Invalid Command", -1, -1);
    }

    private static boolean isInteger(String input) {
        int inputLength = input.length();
        for (int i = 0; i < inputLength; i++) {
            if (input.charAt(i) < '0' || input.charAt(i) > '9') {
                return false;
            }
        }
        return true;
    }
}

package input;

public class Command {
    private String action;
    private int xCoordinate;
    private int yCoordinate;

    Command(String action, int x, int y) {
        if (x < 0 || y < 0 || (!"flag".equals(action) && !"open".equals(action))) {
            this.action = "Invalid Command";
            xCoordinate = -1;
            yCoordinate = -1;
        } else {
            this.action = action;
            xCoordinate = x;
            yCoordinate = y;
        }
    }

    public String getAction() {
        return action;
    }

    public int getxCoordinate() {
        return xCoordinate;
    }

    public int getyCoordinate() {
        return yCoordinate;
    }
}

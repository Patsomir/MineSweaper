package gamecomponents;

import org.junit.Assert;
import org.junit.Test;

public class MineFieldTest {

    @Test
    public void MineField_withArguments10and8_heigthIs10() {
        MineField newField = new MineField(8, 10);
        Assert.assertEquals(10, newField.getHeight());
    }

    @Test
    public void MineField_withArguments10and8_widthIs8() {
        MineField newField = new MineField(8, 10);
        Assert.assertEquals(8, newField.getWidth());
    }

    @Test
    public void MineField_withArbitraryArguments_mineCountIsZero() {
        MineField newField = new MineField(8, 10);
        Assert.assertEquals(0, newField.getMineCount());
    }

    @Test
    public void MineField_withArbitraryArguments_noOpenedBoxes() {
        MineField newField = new MineField(8, 10);
        Assert.assertEquals(0, newField.getOpenedBoxesCount());
    }

    @Test
    public void MineField_withArbitraryArguments_fieldHasNotExploded() {
        MineField newField = new MineField(8, 10);
        Assert.assertFalse(newField.hasExploded());
    }

    @Test
    public void placeMine_whenInvokedOnAnEmptyField_mineCountIsOne() {
        MineField newField = new MineField(3, 4);
        newField.placeMine(2, 3);
        Assert.assertEquals(1, newField.getMineCount());
    }

    @Test
    public void placeMine_whenInvokedOnAnEmptyFieldOnTheSameBoxTwice_mineCountIsOne() {
        MineField newField = new MineField(3, 4);
        newField.placeMine(2, 3);
        newField.placeMine(2, 3);
        Assert.assertEquals(1, newField.getMineCount());
    }

    @Test
    public void placeMine_whenInvokedOnAnEmptyField_returnTrue() {
        MineField newField = new MineField(3, 4);
        Assert.assertTrue(newField.placeMine(2, 3));
    }

    @Test
    public void placeMine_whenInvokedOnAnEmptyFieldOnTheSameBoxTwice_returnFlase() {
        MineField newField = new MineField(3, 4);
        newField.placeMine(2, 3);
        Assert.assertFalse(newField.placeMine(2, 3));
    }

    @Test
    public void toggleFlag_whenInvokedOnAnEmptyField_returnTrue() {
        MineField newField = new MineField(3, 4);
        Assert.assertTrue(newField.toggleFlag(2, 3));
    }

    @Test
    public void toggleFlag_whenInvokedOnAnEmptyFieldOnTheSameBoxTwice_returnTrue() {
        MineField newField = new MineField(3, 4);
        newField.toggleFlag(2, 3);
        Assert.assertTrue(newField.toggleFlag(2, 3));
    }

    @Test
    public void openBox_whenInvokedOnAnUnopenedBox_returnTrue() {
        MineField newField = new MineField(3, 4);
        Assert.assertTrue(newField.openBox(2, 3));
    }

    @Test
    public void openBox_whenInvokedOnAnUnopenedNonSafeBox_incrementOpenBoxes() {
        MineField newField = new MineField(3, 4);
        newField.placeMine(2, 2);
        newField.openBox(2, 3);
        Assert.assertEquals(1, newField.getOpenedBoxesCount());
    }

    @Test
    public void openBox_whenInvokedOnAnOpenedBox_returnFalse() {
        MineField newField = new MineField(3, 4);
        newField.openBox(2, 3);
        Assert.assertFalse(newField.openBox(2, 3));
    }

    @Test
    public void openBox_whenInvokedOnAnOpenedBox_OpenBoxesRemainTheSame() {
        MineField newField = new MineField(3, 4);
        newField.placeMine(2, 2);
        newField.openBox(2, 3);
        newField.openBox(2, 3);
        Assert.assertEquals(1, newField.getOpenedBoxesCount());
    }

    @Test
    public void openBox_whenInvokedOnAMinedBox_detonateMines() {
        MineField newField = new MineField(3, 4);
        newField.placeMine(2, 2);
        newField.openBox(2, 2);
        Assert.assertTrue(newField.hasExploded());
    }

    @Test
    public void openBox_whenInvokedOnANonMinedBox_MinesHaveNotExploded() {
        MineField newField = new MineField(3, 4);
        newField.placeMine(2, 2);
        newField.openBox(2, 3);
        Assert.assertFalse(newField.hasExploded());
    }

    @Test
    public void openBox_whenInvokedOnAFlagedBox_returnFalse() {
        MineField newField = new MineField(3, 4);
        newField.toggleFlag(2, 2);
        Assert.assertFalse(newField.openBox(2, 2));
    }

    @Test
    public void openBox_whenInvokedOnAnEmptyThreeOnThreeField_openAllBoxes() {
        MineField newField = new MineField(3, 3);
        newField.openBox(1, 1);
        Assert.assertEquals(9, newField.getOpenedBoxesCount());
    }

    @Test
    public void openBox_whenInvokedOnSafeBoxOfAThreeOnThreeFieldWithOneMine_openEightBoxes() {
        MineField newField = new MineField(3, 3);
        newField.placeMine(0, 0);
        newField.openBox(2, 2);
        Assert.assertEquals(8, newField.getOpenedBoxesCount());
    }
}

package gamecomponents;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;

import org.junit.Assert;
import org.junit.Test;

public class MineBoxTest {

    @Test
    public void MineBox_whenNewMineBoxCreated_mineIsFalse() {
        MineBox newBox = new MineBox();
        Assert.assertFalse(newBox.hasMine());
    }

    @Test
    public void MineBox_whenNewMineBoxCreated_flagIsFalse() {
        MineBox newBox = new MineBox();
        Assert.assertFalse(newBox.hasFlag());
    }

    @Test
    public void MineBox_whenNewMineBoxCreated_openIsFalse() {
        MineBox newBox = new MineBox();
        Assert.assertFalse(newBox.isOpen());
    }

    @Test
    public void MineBox_whenNewMineBoxCreated_dangerLevelIsZero() {
        MineBox newBox = new MineBox();
        Assert.assertEquals(0, newBox.getDangerLevel());
    }

    @Test
    public void increaseDangerLevel_dangerLevelIsZeroAndMethodInvoked_increaseDangerLevelByOne() {
        MineBox safeBox = new MineBox();
        safeBox.increaseDangerLevel();
        Assert.assertEquals(1, safeBox.getDangerLevel());
    }

    @Test
    public void increaseDangerLevel_dangerLevelIsZeroAndMethodInvokedThreeTimes_increaseDangerLevelByThree() {
        MineBox safeBox = new MineBox();
        safeBox.increaseDangerLevel();
        safeBox.increaseDangerLevel();
        safeBox.increaseDangerLevel();
        Assert.assertEquals(3, safeBox.getDangerLevel());
    }

    @Test
    public void placeMine_mineIsFlase_mineBecomesTrue() {
        MineBox safeBox = new MineBox();
        safeBox.placeMine();
        Assert.assertTrue(safeBox.hasMine());
    }

    @Test
    public void placeMine_mineIsTrue_mineRemainsTrue() {
        MineBox unsafeBox = new MineBox();
        unsafeBox.placeMine();
        Assert.assertTrue(unsafeBox.hasMine());
    }

    @Test
    public void placeMine_mineIsFlaseAndMethodInvoked_returnTrue() {
        MineBox safeBox = new MineBox();
        Assert.assertTrue(safeBox.placeMine());
    }

    @Test
    public void placeMine_mineIsTrueAndMethodInvoked_returnFalse() {
        MineBox unsafeBox = new MineBox();
        unsafeBox.placeMine();
        Assert.assertFalse(unsafeBox.placeMine());
    }

    @Test
    public void toggleFlag_flagIsFalse_flagBecomesTrue() {
        MineBox box = new MineBox();
        box.toggleFlag();
        Assert.assertTrue(box.hasFlag());
    }

    @Test
    public void toggleFlag_flagIsTrue_flagBecomesFalse() {
        MineBox box = new MineBox();
        box.toggleFlag();
        box.toggleFlag();
        Assert.assertFalse(box.hasFlag());
    }

    @Test
    public void toggleFlag_boxIsNotOpenAndMethodInvoked_returnTrue() {
        MineBox box = new MineBox();
        Assert.assertTrue(box.toggleFlag());
    }

    @Test
    public void toggleFlag_boxIsOpenAndMethodInvoked_returnFalse() {
        MineBox box = new MineBox();
        box.openBox();
        Assert.assertFalse(box.toggleFlag());
    }

    @Test
    public void openBox_boxIsNotOpen_boxBecomesOpen() {
        MineBox closedBox = new MineBox();
        closedBox.openBox();
        Assert.assertTrue(closedBox.isOpen());
    }

    @Test
    public void openBox_boxIsOpen_boxRemainsOpen() {
        MineBox openBox = new MineBox();
        openBox.openBox();
        Assert.assertTrue(openBox.isOpen());
    }

    @Test
    public void openBox_boxIsOpenAndMethodInvoked_returnFalse() {
        MineBox openBox = new MineBox();
        openBox.openBox();
        Assert.assertFalse(openBox.openBox());
    }

    @Test
    public void openBox_boxHasFlagAndMethodInvoked_returnFalse() {
        MineBox flaggedBox = new MineBox();
        flaggedBox.toggleFlag();
        Assert.assertFalse(flaggedBox.openBox());
    }

    @Test
    public void openBox_boxIsNotOpenAndhasNoFlagAndMethodInvoked_returnTrue() {
        MineBox closedBox = new MineBox();
        Assert.assertTrue(closedBox.openBox());
    }

    @Test
    public void print_flaggedBox_printFlag() {
        MineBox box = new MineBox();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        box.toggleFlag();
        box.print();
        Assert.assertEquals("X", output.toString());
    }

    @Test
    public void print_unopenedBox_printWhiteSquare() {
        MineBox box = new MineBox();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        box.print();

        Assert.assertEquals("\u25A0", output.toString());
    }

    @Test
    public void print_safeOpenedBox_printBlackSquare() {
        MineBox box = new MineBox();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        box.openBox();
        box.print();

        Assert.assertEquals("\u25A1", output.toString());
    }

    @Test
    public void print_unsafeOpenedBox_printDangerLevel() {
        MineBox box = new MineBox();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        box.increaseDangerLevel();
        box.openBox();
        box.print();

        Assert.assertEquals("1", output.toString());
    }

    @Test
    public void print_minedBox_printMine() {
        MineBox box = new MineBox();
        ByteArrayOutputStream output = new ByteArrayOutputStream();
        System.setOut(new PrintStream(output));

        box.placeMine();
        box.openBox();
        box.print();

        Assert.assertEquals("\u2666", output.toString());
    }
}

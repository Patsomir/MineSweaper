package input;

import org.junit.Assert;
import org.junit.Test;


public class CommandTest {

    @Test
    public void Command_whenInvokedWithJumpZeroOne_createInvalidCommand() {
        Command newCommand = new Command("Jump", 0, 1);
        
        Assert.assertEquals("Invalid Command", newCommand.getAction());
    }
    
    @Test
    public void Command_whenInvokedWithOpenZeroMinusOne_createInvalidCommand() {
        Command newCommand = new Command("open", 0, -1);
        
        Assert.assertEquals("Invalid Command", newCommand.getAction());
    }
    
    @Test
    public void Command_whenInvokedWithOpenZeroOne_createValidOpenCommand() {
        Command newCommand = new Command("open", 0, 1);
        
        Assert.assertEquals("open", newCommand.getAction());
        Assert.assertEquals(0, newCommand.getxCoordinate());
        Assert.assertEquals(1, newCommand.getyCoordinate());
    }

    @Test
    public void Command_whenInvokedWithFlagZeroOne_createValidFlagCommand() {
        Command newCommand = new Command("flag", 0, 1);
        
        Assert.assertEquals("flag", newCommand.getAction());
        Assert.assertEquals(0, newCommand.getxCoordinate());
        Assert.assertEquals(1, newCommand.getyCoordinate());
    }
}
